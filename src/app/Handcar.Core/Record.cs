using System;

namespace Handcar.Core
{
  public abstract class Record<T> : IComparable<T>, IEquatable<T>
  {
    public int CompareTo(T other) => throw new NotImplementedException();

    public bool Equals(T other) => throw new NotImplementedException();
  }

  public class ImmutableRecord<T> : Record<T>
  {

  }

  public class MutableRecord<T> : Record<T>
  {
    
  }
}