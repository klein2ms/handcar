using System;
using System.Collections.Generic;
using System.Linq;

namespace Handcar.Core
{
  public static class EnumerableExtensions
  {
    public static IEnumerable<TResult> Choose<T, TResult>(
      this IEnumerable<T> seq,
      Func<T, Option<TResult>> map) =>
        seq.Select(map)
          .OfType<Some<TResult>>()
          .Select(x => (TResult)x);

    public static Option<T> TryFind<T>(
      this IEnumerable<T> seq,
      Func<T, bool> predicate) =>
        seq.Where(predicate)
          .Select<T, Option<T>>(x => x)
          .DefaultIfEmpty(None.Value)
          .First();
  }
}