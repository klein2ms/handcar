using System;

namespace Handcar.Core
{
  public abstract class Result<TSuccess, TFailure>
  {
    public static implicit operator Result<TSuccess, TFailure>(TSuccess result) => new Success<TSuccess, TFailure>(result);

    public static implicit operator Result<TSuccess, TFailure>(TFailure result) => new Failure<TSuccess, TFailure>(result);
  }

  public class Success<TSuccess, TFailure> : Result<TSuccess, TFailure>
  {
    public static implicit operator TSuccess(Success<TSuccess, TFailure> result) => result._data;

    private TSuccess _data { get; }

    public Success(TSuccess data) => _data = data;
  }

  public class Failure<TSuccess, TFailure> : Result<TSuccess, TFailure>
  {
    public static implicit operator TFailure(Failure<TSuccess, TFailure> result) => result._data;

    private TFailure _data { get;  }

    public Failure(TFailure data) => _data = data;
  }

  public static class ResultAdapters
  {
     public static Result<TNextSuccess, TFailure> TryCatch<TSuccess, TFailure, TNextSuccess>(
      Result<TSuccess, TFailure> result,
      Func<TSuccess, TNextSuccess> tryCatch,
      Func<Exception, TFailure> failWith)
    {
      try
      {
        return result is Success<TSuccess, TFailure> success
          ? (Result<TNextSuccess, TFailure>)tryCatch(success)
          : (TFailure)(Failure<TSuccess, TFailure>)result;
      }
      catch (Exception e)
      {
        return (TFailure)(Failure<TSuccess, TFailure>)failWith(e);
      }
    }
  }

  public static class ResultExtensions
  {
    public static Result<TSuccess, TNextFailure> Bind<TSuccess, TFailure, TNextFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TFailure, TNextFailure> lift) =>
        result is Failure<TSuccess, TFailure> failure
          ? (Result<TSuccess, TNextFailure>)lift(failure)
          : (TSuccess)(Success<TSuccess, TFailure>)result;

    public static Result<TSuccess, TNextFailure> Bind<TSuccess, TFailure, TNextFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TFailure, Result<TSuccess, TNextFailure>> lift) =>
        result is Failure<TSuccess, TFailure> failure
          ? lift(failure)
          : (TSuccess)(Success<TSuccess, TFailure>)result;

    public static Result<TNextSuccess, TFailure> Map<TSuccess, TFailure, TNextSuccess>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, TNextSuccess> map) =>
        result is Success<TSuccess, TFailure> success
          ? (Result<TNextSuccess, TFailure>)map(success)
          : (TFailure)(Failure<TSuccess, TFailure>)result;

    public static Result<TNextSuccess, TNextFailure> DoubleMap<TSuccess, TFailure, TNextSuccess, TNextFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, TNextSuccess> successFunc,
      Func<TFailure, TNextFailure> failureFunc) =>
        result is Success<TSuccess, TFailure> success
          ? (Result<TNextSuccess, TNextFailure>)successFunc(success)
          : (Result<TNextSuccess, TNextFailure>)failureFunc((TFailure)(Failure<TSuccess, TFailure>)result);


    public static Result<TSuccess, TFailure> Plus<TSuccess, TFailure>(
      this Result<TSuccess, TFailure> resultOne,
      Result<TSuccess, TFailure> resultTwo,
      Func<TSuccess, TSuccess, TSuccess> reduceSuccesses,
      Func<TFailure, TFailure, TFailure> reduceFailures)
    {
      if (resultOne is Success<TSuccess, TFailure> successOne && resultTwo is Success<TSuccess, TFailure> successTwo)
        return (Result<TSuccess, TFailure>)reduceSuccesses(successOne, successTwo);

      if (resultOne is Failure<TSuccess, TFailure> failureOne && resultTwo is Success<TSuccess, TFailure>)
        return failureOne;

      if (resultOne is Success<TSuccess, TFailure> && resultTwo is Failure<TSuccess, TFailure> failureTwo)
        return failureTwo;

      return reduceFailures(
          (TFailure)(Failure<TSuccess, TFailure>)resultOne,
          (TFailure)(Failure<TSuccess, TFailure>)resultTwo);
    }

    public static Result<TSuccess, TFailure> Tee<TSuccess, TFailure>(
      this Result<TSuccess, TFailure> result,
      Action<Result<TSuccess, TFailure>> tee)
    {
      tee(result);
      return result;
    }

    public static Result<TNextSuccess, TFailure> TryCatch<TSuccess, TFailure, TNextSuccess>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, TNextSuccess> tryCatch,
      Func<Exception, TFailure> failWith) =>
        ResultAdapters.TryCatch(result, tryCatch, failWith);

    public static Result<TSuccess, TFailure> Compose<TSuccess, TFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, Result<TSuccess, TFailure>> switchFunc) =>
        result is Success<TSuccess, TFailure> success
          ? switchFunc(success)
          : (Failure<TSuccess, TFailure>)result;

    public static TFailure Reduce<TSuccess, TFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, TFailure> map) =>
        result is Success<TSuccess, TFailure> success
          ? map(success)
          : (Failure<TSuccess, TFailure>)result;

    public static Result<TSuccess, TFailure> Filter<TSuccess, TFailure>(
      this Result<TSuccess, TFailure> result,
      Func<TSuccess, TFailure> map,
      Func<TSuccess, bool> filter) =>
        result is Success<TSuccess, TFailure> bound && filter(bound)
          ? (Result<TSuccess, TFailure>)map(bound)
          : result;
  }
}