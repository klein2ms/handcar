using System;

namespace Handcar.Core
{
  public abstract class Option<T>
  {
    public static implicit operator Option<T>(T value) => new Some<T>(value);

    public static implicit operator Option<T>(None none) => new None<T>();
  }

  public class Some<T> : Option<T>
  {
    public static implicit operator T(Some<T> value) => value._value;

    private T _value { get; }

    public Some(T value) => _value = value;
  }

  public class None<T> :  Option<T> {  }

  public class None
  {
    public static None Value { get; } = new None();

    private None() {}
  }

  public static class OptionExtensions
  {
    public static Option<TResult> Map<T, TResult>(this Option<T> option, Func<T, TResult> map) =>
      option is Some<T> some ? (Option<TResult>)map(some) : None.Value;

    public static Option<T> Filter<T>(this T value, Func<T, bool> predicate) =>
      predicate(value) ? (Option<T>)value : None.Value;

    public static T Choose<T>(this Option<T> option, T whenNone) =>
      option is Some<T> some ? (T)some : whenNone;

    public static T Choose<T>(this Option<T> option, Func<T> whenNone) =>
      option is Some<T> some ? (T)some : whenNone();
  }
}